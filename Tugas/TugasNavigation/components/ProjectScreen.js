import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';

export const ProjectScreen = ({navigation}) => (
  <View style={styles.container}>
    <Text style={{fontSize: 36, textAlign: 'center', padding: 20}}>
      Halaman Project
    </Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
