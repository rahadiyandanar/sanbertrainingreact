import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import {LoginScreen} from './LoginScreen';
import {AboutScreen} from './AboutScreen';
import {SkillScreen} from './SkillScreen';
import {AddScreen} from './components/AddScreen';
import {ProjectScreen} from './components/ProjectScreen';

const HomeStack = createStackNavigator();
const AboutStack = createStackNavigator();
const DetailStack = createStackNavigator();
const Tabs = createMaterialBottomTabNavigator();
const Drawer = createDrawerNavigator();

const HomeStackScreen = () => (
  <HomeStack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <HomeStack.Screen name="Home" component={LoginScreen} />
  </HomeStack.Navigator>
);

const DetailsStackScreen = () => (
  <DetailStack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <DetailStack.Screen name="Details" component={TabsDetail} />
  </DetailStack.Navigator>
);

const AboutStackScreen = () => (
  <AboutStack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <AboutStack.Screen name="About" component={AboutScreen} />
  </AboutStack.Navigator>
);

const TabsDetail = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={SkillScreen} />
    <Tabs.Screen name="Project" component={ProjectScreen} />
    <Tabs.Screen name="Add" component={AddScreen} />
  </Tabs.Navigator>
);

export default class Index extends Component {
  render() {
    return (
      <NavigationContainer>
        <Drawer.Navigator>
          <Drawer.Screen name="Home" component={HomeStackScreen} />
          <Drawer.Screen name="About" component={AboutStackScreen} />
          <Drawer.Screen name="Details" component={DetailsStackScreen} />
        </Drawer.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
