import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  TextInput,
  Button,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoImage}>
          <Image
            source={require('./images/ReactLogo.png')}
            style={{
              width: 106,
              height: 94,
            }}
          />
        </View>
        <Text
          style={{
            color: '#FFFFFF',
            textAlign: 'center',
            paddingTop: 20,
            fontSize: 36,
          }}>
          Sign In
        </Text>
        <View style={styles.loginForm}>
          <Text style={styles.labelText}>Username</Text>
          <TextInput
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              color: '#FFFFFF',
            }}
          />
          <Text style={styles.labelText}>Password</Text>
          <TextInput
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              color: '#FFFFFF',
            }}
          />
          <Text
            style={{
              color: '#FFFFFF',
              textAlign: 'right',
              paddingTop: 10,
              paddingBottom: 40,
            }}>
            Forgot Password
          </Text>

          <Button
            title="Sign In"
            onPress={() => Alert.alert('Simple Button pressed')}
            style={{color: '#03DAC6'}}
          />
        </View>
        <View
          style={{
            alignItems: 'center',
            paddingTop: 700,
            alignSelf: 'center',
            position: 'absolute',
          }}>
          <View style={styles.oval} />
        </View>
        <View
          style={{
            alignItems: 'center',
            paddingTop: 740,
            alignSelf: 'center',
            position: 'absolute',
          }}>
          <View style={styles.oval} />
        </View>
        <View
          style={{
            alignItems: 'center',
            paddingTop: 780,
            alignSelf: 'center',
            position: 'absolute',
          }}>
          <View style={styles.oval} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#171F24',
  },

  logoImage: {
    paddingTop: 100,
    alignItems: 'center',
  },
  labelText: {
    color: '#FFFFFF',
    paddingTop: 20,
  },

  loginForm: {
    padding: 40,
  },

  oval: {
    width: 220,
    height: 300,
    borderRadius: 200,
    backgroundColor: '#44B2F9',
    opacity: 0.08,
    transform: [{scaleX: 2}],
  },
});
