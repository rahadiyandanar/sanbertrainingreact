import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  TextInput,
  Button,
  Alert,
  LinearGradient,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class SkillScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <TouchableOpacity>
            <View style={{flexDirection: 'row'}}>
              <Icon
                name="arrow-back"
                size={20}
                style={{color: '#FFFFFF', paddingRight: 7}}
              />
              <Text
                style={{
                  color: '#FFFFFF',
                  textAlign: 'center',
                }}>
                Back
              </Text>
            </View>
          </TouchableOpacity>
          <Text
            style={{
              color: '#FFFFFF',
              textAlign: 'center',
              fontSize: 16,
            }}>
            Skillset
          </Text>
          <TouchableOpacity>
            <Text
              style={{
                color: '#FFFFFF',
                textAlign: 'center',
              }}>
              Sign Out
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.headerBG} />
        <View
          style={{
            flexDirection: 'row',
            paddingLeft: 30,
            paddingTop: 20,
            paddingBottom: 20,
          }}>
          <Icon name="person" size={108} style={{color: '#FFFFFF'}} />
          <View style={{flexDirection: 'column', paddingLeft: 20}}>
            <Text
              style={{
                color: '#FFFFFF',
                fontSize: 28,
              }}>
              Rich Brian
            </Text>
            <Text
              style={{
                color: '#FFFFFF',
                fontSize: 18,
              }}>
              Software Engineer
            </Text>
            <Icon name="school" size={25} style={{color: '#FFFFFF'}} />
          </View>
        </View>

        <Text style={styles.listTitle}>Programming Language</Text>
        <TouchableOpacity>
          <View style={styles.listItemBox}>
            <Icon name="school" size={40} style={{color: '#FFFFFF'}} />
            <View style={styles.listItem}>
              <Text style={styles.listItemText}>Javascript (Intermediate)</Text>
              <View
                style={{
                  width: 100,
                  height: 7,
                  backgroundColor: '#03DAC6',
                }}
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.listItemBox}>
            <Icon name="school" size={40} style={{color: '#FFFFFF'}} />
            <View style={styles.listItem}>
              <Text style={styles.listItemText}>ABAP (Intermediate)</Text>
              <View
                style={{
                  width: 100,
                  height: 7,
                  backgroundColor: '#03DAC6',
                }}
              />
            </View>
          </View>
        </TouchableOpacity>

        <Text style={styles.listTitle}>Framework / Library</Text>
        <TouchableOpacity>
          <View style={styles.listItemBox}>
            <Icon name="school" size={40} style={{color: '#FFFFFF'}} />
            <View style={styles.listItem}>
              <Text style={styles.listItemText}>React JS (Basic)</Text>
              <View
                style={{
                  width: 100,
                  height: 7,
                  backgroundColor: '#03DAC6',
                }}
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.listItemBox}>
            <Icon name="school" size={40} style={{color: '#FFFFFF'}} />
            <View style={styles.listItem}>
              <Text style={styles.listItemText}>SAPUI5 (Advanced)</Text>
              <View
                style={{
                  width: 100,
                  height: 7,
                  backgroundColor: '#03DAC6',
                }}
              />
            </View>
          </View>
        </TouchableOpacity>

        <Text style={styles.listTitle}>Others</Text>
        <TouchableOpacity>
          <View style={styles.listItemBox}>
            <Icon name="school" size={40} style={{color: '#FFFFFF'}} />
            <View style={styles.listItem}>
              <Text style={styles.listItemText}>Github (Basic)</Text>
              <View
                style={{
                  width: 100,
                  height: 7,
                  backgroundColor: '#03DAC6',
                }}
              />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const W = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#171F24',
  },

  headerBG: {
    position: 'absolute',
    height: 200,
    width: W,
    backgroundColor: '#44B2F9',
    opacity: 0.08,
    borderBottomRightRadius: 100,
    left: -10,
  },

  listTitle: {
    color: '#FFFFFF',
    textAlign: 'left',
    paddingLeft: 40,
    paddingTop: 30,
    fontSize: 20,
  },

  listItemBox: {
    flexDirection: 'row',
    paddingLeft: 50,
    paddingTop: 30,
  },
  listItem: {
    flexDirection: 'column',
    paddingLeft: 20,
  },

  listItemText: {
    color: '#FFFFFF',
    paddingBottom: 5,
  },

  navBar: {
    height: 55,
    backgroundColor: '#171F24',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  logoImage: {
    paddingTop: 100,
    alignItems: 'center',
  },
  labelText: {
    color: '#FFFFFF',
    paddingTop: 20,
  },

  loginForm: {
    padding: 40,
  },
});
