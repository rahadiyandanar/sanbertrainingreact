import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  TextInput,
  Button,
  Alert,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <TouchableOpacity>
            <Icon name="search" size={60} style={{color: '#171F24'}} />
          </TouchableOpacity>
          <Text
            style={{
              color: '#FFFFFF',
              textAlign: 'center',
              fontSize: 16,
            }}>
            About
          </Text>
          <TouchableOpacity>
            <Text
              style={{
                color: '#FFFFFF',
                textAlign: 'center',
              }}>
              Sign Out
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',

            paddingLeft: 30,
            paddingTop: 20,
            paddingBottom: 20,
          }}>
          <View style={styles.headerBG} />
          <Icon name="person" size={108} style={{color: '#FFFFFF'}} />
          <View style={{flexDirection: 'column', paddingLeft: 20}}>
            <Text
              style={{
                color: '#FFFFFF',
                //   textAlign: 'left',
                //   paddingTop: 20,
                fontSize: 28,
              }}>
              Rich Brian
            </Text>
            <Text
              style={{
                color: '#FFFFFF',
                //   textAlign: 'left',
                fontSize: 18,
              }}>
              Software Engineer
            </Text>
            <Icon name="school" size={25} style={{color: '#FFFFFF'}} />
          </View>
        </View>
        <Text
          style={{
            color: '#FFFFFF',
            textAlign: 'left',
            paddingLeft: 40,
            paddingRight: 30,
            paddingTop: 100,
            fontSize: 18,
          }}>
          Hello, this is my first RN mockup using FIGMA. Please check my
          skillset
        </Text>
        <Text
          style={{
            color: '#FFFFFF',
            textAlign: 'left',
            paddingLeft: 40,
            paddingTop: 30,
            fontSize: 20,
          }}>
          Project
        </Text>
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              paddingLeft: 50,
              paddingTop: 20,
            }}>
            <Icon name="school" size={40} style={{color: '#FFFFFF'}} />
            <View style={{flexDirection: 'column', paddingLeft: 20}}>
              <Text
                style={{
                  color: '#FFFFFF',
                }}>
                Next JS Blog
              </Text>
              <Text
                style={{
                  color: '#FFFFFF',
                }}>
                Training Project of Next JS
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              // alignItems: 'center',
              paddingLeft: 50,
              paddingTop: 30,
            }}>
            <Icon name="school" size={40} style={{color: '#FFFFFF'}} />
            <View style={{flexDirection: 'column', paddingLeft: 20}}>
              <Text
                style={{
                  color: '#FFFFFF',
                }}>
                Next JS Blog
              </Text>
              <Text
                style={{
                  color: '#FFFFFF',
                }}>
                Training Project of Next JS
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              // alignItems: 'center',
              paddingLeft: 50,
              paddingTop: 30,
            }}>
            <Icon name="school" size={40} style={{color: '#FFFFFF'}} />
            <View style={{flexDirection: 'column', paddingLeft: 20}}>
              <Text
                style={{
                  color: '#FFFFFF',
                }}>
                Next JS Blog
              </Text>
              <Text
                style={{
                  color: '#FFFFFF',
                }}>
                Training Project of Next JS
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const W = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#171F24',
  },

  headerBG: {
    position: 'absolute',
    height: 200,
    width: W,
    backgroundColor: '#44B2F9',
    opacity: 0.08,
    borderBottomRightRadius: 100,
    left: -10,
  },

  navBar: {
    height: 55,
    backgroundColor: '#171F24',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
