/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import YoutubeUI from './Tugas/Tugas12/App';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import SkillScreen from './Tugas/Tugas13/SkillScreen';
import ToDoApp from './Tugas/Tugas14/App';
import ReactNavigationApp from './Tugas/Tugas15/index';
import NavigationTask from './Tugas/TugasNavigation/index';
import Quiz3 from './Tugas/Quiz3/index';

const App: () => React$Node = () => {
  return (
    <>
      {/* <YoutubeUI /> */}
      {/* <LoginScreen /> */}
      {/* <AboutScreen /> */}
      {/* <SkillScreen /> */}
      {/* <ToDoApp /> */}
      {/* <ReactNavigationApp /> */}
      {/* <NavigationTask /> */}
      <Quiz3 />
    </>
  );
};

export default App;
